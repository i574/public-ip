#!/bin/bash

# git pull

CURRENT_IPADDR=$(curl ifconfig.me)
RECORDED_IPADDR=$(cat ./ip.txt)

if [ "$CURRENT_IPADDR" == "$RECORDED_IPADDR" ]; then
  echo "IP address is current."
else
  echo "IP address is not current, updating."
  echo $CURRENT_IPADDR > ip.txt
  git add -A
  git commit -m 'updated IP address'
  git push
fi
